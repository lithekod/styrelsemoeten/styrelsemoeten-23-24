#import "template.typ": *

#show: meeting.with(
  date: "2023-10-04",
)

// We generated the example code below so you can see how
// your document will look. Go ahead and replace it with
// your own content!

#issues([
  #issue([Mindroad workshop])

  - Vi har ställt in det tidigare bestämda datumet pga. för nära på NCPC
  - Nytt datum 8 november
    - Krockar ej med meetup

  #issue([SICK spons])

  - Förhoppningsvis ny sponsor, SICK
  - Okänd LiTHe kod-bekant jobbar på SICK, de har tagit kontakt med oss och vill gärna hålla ett evenemang med oss, hos dem
  - Hon hade lite punkter att ta up:
    - De vill bjuda oss till sitt kontor så de kan visa upp coola grejer
    - Slutet på januari, tidig februari, kunde vara en rimlig tid
    - Hur går vi vidare?
      - Vi tycker att runt slutet av januari, början av februari, låter jättebra
    - Hur ser er marknadsföringsplan ut? När vill ni ha input till den?
      - 1 månad innan gör vi
        - ett facebook-evenemang
        - ett evenemang på orbi
        - en announcement i Discord
        - planscher som vi sätter ut på universitetet
      - Sponsorbild på hemsidan
    - Vad kostar det?
      - 20 000 kr
    - Hur många kan man tänka sig skulle vara intresserade?
      - Uppskattar 10-20
    - Vilken dag på veckan är lämplig?
      - Tisdagar kan vara lämpliga eller inte beroende på hur man ser det. Det krockar ju med meetups men det kan ju också vara rimligt att ha evenemang hos er istället just den veckan.
      - Torsdagar kan vara en annan bra dag
    - Har ni rentav redan nu ett förslag på ett datum som ser bra ut?
      - 23 januari
      - 25 januari
      - 30 januari
      - 1 februari
      - 6 februari
      - 8 februari
    - Är det något särskilt som ni önskar från oss eller av ett besök här?
      - Få se schackrobot om den är ihopsatt
      - Få se coola projekt ni har på era maskiner
      - Få se andra coola grejer ;)

  #issue([Nycket i förrådet])

  - Gör det möjligt för andra medlemmar att komma in
  - Borttappad nyckel kostar 1000 kr
  - Vad kostar en extra nyckel

  #issue([Inköpslista till rummet])

  - Soffa
  - Hyllor
  - Mattor
  - Städsaker
  - Flugsmälla
  - Mjuka akustikplattor
  - Kontroller (För att spela på pi:en)
  - Arbetsbänk
  - Hatthylla
  - Gardiner
])
