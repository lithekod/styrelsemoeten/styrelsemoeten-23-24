#import "template.typ": *

#show: meeting.with(
  date: "December 13, 2023",
)

#participants(
  (
    ([Henry Andersson], [Ordförande]),
    ([Emanuel Särnhammar], [Kassör]),
    ([Hamza Keifo], [Verksamhetsledare]),
    ([Hannah Bahrehman], [Styrelseledamot]),
    ([Gustav Sörnäs], [Hårdvarugruppen]),
    ([Martin], [Game Jam-gruppen]),
  )
)

= Möte

== Mötet börjar

Mötet inleds 12:28.

#issues([
  #issue([Ledningströjor])

  - Vi behöver gå till Marknadsbyrån med de feltryckta tröjorna
  - Emanuel orkar inte, Henry kan gå

  #issue([NCPC-tröjor])

  - Vi behöver köpa tröjor för NCPC
  - Svårt att kontakta Lowe
  - Vi bestämmer att tröjan ska vara mörkgrön

  #issue([Inköpslista till rummet])

  - Hannah har inte gjort en inköpslista till rummet, ska göra när det blir mindre stressigt.
  - Soffan bör göras rent eller ersättas med en annan.

  #issue([SICK])

  - SICK har inte återkommit sedan senaste mejlet

  #issue([Planera möte med undergrupper])

  - Borde ske någon gång i januari

  #issue([Advent of Code-leaderboard])

  - Simon råkade ta bort filtret för icke-tävlande
    - Det borde fixas, är missvisande just nu, men Simon gör det när han har tid
  - Emanuel föreslår att göra om leaderboarden
    - Ha icke-tävlande med i topplistan fast utan numrering
    - Möjlighet till att visa andra universitets kodföreningar och jämföra med

  #issue([Föreningar för föreingsaktivas])

  - Gustav var på föreningsaktivas stormöte
  - Hugo och Gunnar har stått för det, de kommer snart försvinna
  - Skulle vara bra med en samlingspunkt för föreningar på LiU
    - Föreningar har inte mycket säg ensamma, starkare tillsammans
  - Ingen i LiTHe kods styrelse är intresserad av att engagera sig
  - Orbi
    - Sektioner tycker att det suger
    - Mobil och konto är måste för att använda
    - Lysator har skrivit ett manifesto om att det suger
])

== Mötet avslutas

Mötet avslutas 12:57.
