#let participants(ps) = { 
  [= Deltagare]

  table(
    columns: (auto, auto, auto),
    stroke: none,
    ..(ps.map(p => (p.at(0), [-], p.at(1))).flatten()))
}

#let issues(body) = {
  set heading(numbering: ((..a) => [#a.pos().at(1, default: none).]))
  body
  set heading(numbering: none)
}

#let issue(name) = { [== #name <issue>]}

#let meeting(authors: (), date: none, body) = {
  let title = "Styrelsemöte"

  set document(author: authors, title: title)
  set page(numbering: "1", number-align: center)
  set text(font: "Linux Libertine", lang: "en")

  // Title row.
  align(center)[
    #block(text(weight: 700, 1.75em, [LiTHe kod]))
    #block(text(weight: 700, 1.75em, title))
    #v(1em, weak: true)
    #date
  ]

  // Main body.
  set par(justify: true)

  let issue = label("issue")

  locate(loc => {
    let issues = query(issue, loc)
    [= Mötespunkter]
    enum(..issues.map(h => h.body))
  })

  body
}
