#import "template.typ": *

#show: meeting.with(
  date: "1 juni 2024",
)

#participants(
  (
    ([Henry Andersson], [Ordförande]),
    ([Emanuel Särnhammar], [Kassör]),
    ([Simon Gutgesell], [Styrelseledamot]),
    // ([Hannah Bahrehman], [Styrelseledamot]),
  )
)

= Möte

== Mötet börjar

Mötet inleds 13:30.

#issues([
  #issue([Hitta ny styrelse])

  - Vi har några kandidater
    - Leo
    - Morgan
    - Kacper
    - Herman
    - \@Mafrans
  - Vi ska skicka ut intresseanmälan
    - Skicka ut till våra kandidater privat och announcements
      - Henry skickar till kandidater
      - Vi ska skicka i announcements

        "Vill du vara med i nästa års styrelse för LiTHe kod? Här kan du intresseanmäla dig!"

    - Vi kan göra stormötet till ett event
      
      Stormöte

  #issue([Skicka ut "Vill du fortfarande vara medlem?"])

  - Det borde kanske göras?
  - Simon fixar någon gång.

  #issue([Vårmöte försök 2: Electric Boogaloo])

  - Boka in nytt möte
    - 2 juli är ett okej datum, om 4 veckor och på en tisdag

  - Boka sal
    - Ska vi boka en vanlig sal eller Ada?
    - Man kan boka en sal tidigast 3 veckor i förväg. Henry bokar salen 11 juni
      för ett möte på den 2 juli.

  - Nya mötespunkter
    1. Val av mötesordförande
    2. Val av mötessekreterare
    3. Val av justeringsperson tillika rösträknare
    4. Fastställande av röstlängden
    5. Beslut om mötets stadgeenliga utlysande
    6. Fastställande av föredragningslista
    7. Styrelsens verksamhetsberättelse
    8. Styrelsens ekonomiska berättelse
    9. Revisorns preliminära granskning av verksamhetsåret
    10. Val av ordförande 24/25
    11. Val av vice ordförande 24/25
    12. Val av kassör 24/25
    13. Val av verksamhetsledare 24/25
    14. Val av övriga styrelseledamoter
    15. Val av game jam-ansvarig 24/25
    16. Val av revisor 24/25
    17. Fastställande av föreningens budget 24/25
    18. Fastställande av medlemsavgift 24/25
    19. Motioner och propositioner
    20. Övriga frågor

  - Inför stormötet
    - Vi behöver göra en verksamhetsberättelse
    - Emanuel behöver göra en ekonomisk berättelse
    - Emanuel behöver skicka bokföring till Frans för granskning
    - Styrelsen behöver besluta om förslag till poster
    - Styrelsen behöver göra en budget
    - Styrelsen behöver föreslå en ny medlemsavgift
      - Vi föreslår att den fortsätter vara 20 kr

  #issue([Nytt möte])

  - Tisdag 4 juni vid illegal meetup, dvs. \~17.30.
  - Vad gör vi då
    - Skriver verksamhetsberättelse
    - Skapar ny budget
    - Fixar förra mötets protokoll
    - Kollar på intresseanmälan
])

== Mötet avslutas

Mötet avslutas 14:13.
