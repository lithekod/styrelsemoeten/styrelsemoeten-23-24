#import "template.typ": *

#show: meeting.with(
  date: "November 8, 2023",
)

#participants(
  (
    ([Henry Andersson], [Ordförande]),
    ([Emanuel Särnhammar], [Kassör]),
    ([Hamza Keifo], [Verksamhetsledare]),
    ([Hannah Bahrehman], [Styrelseledamot]),
    ([Gustav Sörnäs], [Hårdvarugruppen]),
    ([Martin], [Game Jam-gruppen]),
  )
)

= Möte

#issues([
  #issue([Varför är vi här])
   - För att diskutera relationen mellan undergrupper och styrelsen

  #issue([Feedback i undergrupperkanalen])
   - Låg respons från styrelsen
     - Vet ej är ett svar, styrelsen bör svara inom 24 h.
     - Inget svar om PR
     - Styrelsemöte varannan vecka är inte tillräckligt snabbt för att ta beslut
   - Behövs processer
   - Styrelsen - grupp, kommunikation

  #issue([Läsken])
  - Läsk är inköpt med Game Jams budget
  - Utgår från att det finns kvar

  #issue([Förvaring])
  - Tillgång till saker behöver vara i förrådet, för grupper
  - Kategorisera saker, organisera hyllor
  - Hårdvarugruppen ansvarar över hårdvaran
  - Markera Game Jam-gruppens påsar
  - Sladdar är Game Jams

  #issue([PR])
  - Process för planering av PR
  - Plan för evenemang

  #issue([Styrelsemöteprotokoll])
  - Ska vara offentliga, enligt stadger
  - Länk på hemsidan
  - Det är de inte
  - Anonyma mötesprotokoll, endast roller

  #issue([Game Jam-pengar])
  - Borde finnas egen pengakälla
  - Martin håller med
  - Martin har hört från gamla game jam, hets om egen sponsor
  - Leta efter sponsmöjligheter
])

= Beslut

- Skapa kanaler
- Styrelsen feedback inom 24 h.
- Game Jams grejer
- Flytta tillbaka pins, flytta tillbaka hårdvara
